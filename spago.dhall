{-
Welcome to a Spago project!
You can edit this file as you like.
-}
{ name = "puRefactor"
, dependencies =  [ "console", "effect"
                  , "language-cst-parser"
                  , "node-fs-aff"
                  ]
, packages = ./packages.dhall
, sources = [ "src/**/*.purs", "test/**/*.purs" ]
}
