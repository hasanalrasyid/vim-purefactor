" ============================================================================
" File:        purefactor.vim
" Description: Simple vim plugin to move a purescript function to another file
" Maintainer:  Hasan al Rasyid <hasanalrasyid.har@gmail.com>
" License:     MIT/X11
" ============================================================================


" Init {{{
let s:thisFileType = expand('%:e')

if (exists('loaded_puRefactor')) || ( s:thisFileType != 'purs')
    finish
endif

let loaded_puRefactor = 1

let s:pluginPath = resolve(expand('<sfile>:p:h:h'))
let s:git_dir = s:pluginPath . '/.git'
let s:latestCommit = system('git --git-dir ' . s:git_dir . ' log -1 --pretty=format:"%h"')
let s:indexJs = s:pluginPath . '/' . s:latestCommit . '.js'

if empty(glob(s:indexJs)) " {{{
  let s:spagoLocation = (systemlist('which spago'))[0]
  let s:nodeLocation  = (systemlist('which node'))[0]
  echo 'Can not find compiled version for puRefactor commit '. s:latestCommit
  echo 'Checking for ' . s:spagoLocation
  echo 'Checking for ' . s:nodeLocation
  if empty(glob(s:nodeLocation)) || empty(glob(s:spagoLocation))
    echo 'spago and node command is needed, this is available if you have a working purescript installation'
    echo 'Installation of puRefactor is failed'
    finish
  else
    echo 'installing puRefactor executable (as a nodeJS file) in ' . s:indexJs
    let installLog = system('pushd ' . s:pluginPath . '; spago bundle-app --main Main --to ' . s:latestCommit . '.js; popd')
    echo 'finished installing puRefactor executable'
  endif
endif "}}}

if !exists('g:puRefactor_move') " {{{
  let g:puRefactor_move = 0
endif " }}}

"}}}
" Functions {{{
function! PeelToFile(to)
  let delCmd = system('node ' . s:indexJs . ' ' . line('.') . " " . @% . " " . a:to)
  if g:puRefactor_move
    execute delCmd
  endif
  execute 'redraw!'
endfunction

" }}}

" Command {{{

command! -nargs=+ -complete=file PuRefactor call PeelToFile(<f-args>)

" }}}
