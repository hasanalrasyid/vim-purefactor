#!/usr/bin/env bash

hg archive ~/Desktop/puRefactor.zip -I 'doc' -I 'plugin' -I README.md -I LICENSE.md
