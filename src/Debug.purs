module Debug where

import Prelude
import PureScript.CST.Types (Token(..))


newtype TokenS = TokenS Token

instance tokenShow :: Show TokenS where
  show (TokenS s) = case s of
       (TokLeftParen                                          ) -> "TokLeftParen"
       (TokRightParen                                         ) -> "TokRightParen"
       (TokLeftBrace                                          ) -> "TokLeftBrace"
       (TokRightBrace                                         ) -> "TokRightBrace"
       (TokLeftSquare                                         ) -> "TokLeftSquare"
       (TokRightSquare                                        ) -> "TokRightSquare"
       (TokLeftArrow       sourceStyle                        ) -> "TokLeftArrow      "
       (TokRightArrow      sourceStyle                        ) -> "TokRightArrow     "
       (TokRightFatArrow   sourceStyle                        ) -> "TokRightFatArrow  "
       (TokDoubleColon     sourceStyle                        ) -> "TokDoubleColon    "
       (TokForall          sourceStyle                        ) -> "TokForall         "
       (TokEquals                                             ) -> "TokEquals"
       (TokPipe                                               ) -> "TokPipe"
       (TokTick                                               ) -> "TokTick"
       (TokDot                                                ) -> "TokDot"
       (TokComma                                              ) -> "TokComma"
       (TokUnderscore                                         ) -> "TokUnderscore"
       (TokBackslash                                          ) -> "TokBackslash"
       (TokAt                                                 ) -> "TokAt"
       (TokLowerName       _                           string ) -> "TokLowerName      " <> string
       (TokUpperName       _                           string ) -> "TokUpperName      "
       (TokOperator        _                           string ) -> "TokOperator       "
       (TokSymbolName      _                           string ) -> "TokSymbolName     "
       (TokSymbolArrow     sourceStyle                        ) -> "TokSymbolArrow    "
       (TokHole            string                             ) -> "TokHole           "
       (TokChar            string        char                 ) -> "TokChar           "
       (TokString          string        string2              ) -> "TokString         "
       (TokRawString       string                             ) -> "TokRawString      "
       (TokInt             string        int                  ) -> "TokInt            "
       (TokNumber          string        number               ) -> "TokNumber         "
       (TokLayoutStart     int                                ) -> "TokLayoutStart    "
       (TokLayoutSep       int                                ) -> "TokLayoutSep      "
       (TokLayoutEnd       int                                ) -> "TokLayoutEnd      "

