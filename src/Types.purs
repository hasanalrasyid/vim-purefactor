module Types where

import Prelude

import Data.Maybe (Maybe)
import PureScript.CST.Types (Declaration(..))

type Result =
    { resultStr :: String
    , tokenEnd :: Maybe Int
    , tokenStart :: Maybe Int
    }




newtype DeclS = DeclS (Declaration Void)
instance declShow :: Show DeclS where
  show s = case s of
    DeclS (DeclData e m) -> "DeclData e (Maybe (Tuple sourceToken (Separated (DataCtor e))))"
    DeclS (DeclType e sourceToken typeE) -> "DeclType e sourceToken typeE"
    DeclS (DeclNewtype e sourceToken nameProper typeE) -> "DeclNewtype e sourceToken (Name Proper) typeE"
    DeclS (DeclClass classHead m) -> "DeclClass classHead (Maybe (Tuple sourceToken (NonEmptyArray (Labeled (Name Ident) typeE))))"
    DeclS (DeclInstanceChain separatedInstance) -> "DeclInstanceChain (Separated (Instance e))"
    DeclS (DeclDerive sourceToken m instanceHead) -> "DeclDerive sourceToken (Maybe sourceToken) (InstanceHead e)"
    DeclS (DeclKindSignature sourceToken labeledName) -> "DeclKindSignature sourceToken (Labeled (Name Proper) typeE)"
    DeclS (DeclSignature labeledName) -> "DeclSignature (Labeled (Name Ident) typeE)"
    DeclS (DeclValue valueBindingFields) -> "DeclValue (ValueBindingFields e)"
    DeclS (DeclFixity fixityFields) -> "DeclFixity FixityFields"
    DeclS (DeclForeign sourceToken sourceTokenB foreignE) -> "DeclForeign sourceToken sourceToken (Foreign e)"
    DeclS (DeclRole sourceToken sourceTokenB nameProper nonEmptyArrayTuple) -> "DeclRole sourceToken sourceToken (Name Proper) (NonEmptyArray (Tuple sourceToken Role))"
    DeclS (DeclError e) -> "DeclError e"


fromDeclS :: DeclS -> Declaration Void
fromDeclS (DeclS a) = a
