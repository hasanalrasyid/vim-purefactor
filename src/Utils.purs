module Utils where

import Prelude
import Data.String (joinWith)

import Data.Maybe (Maybe(..))

unwords :: Array String -> String
unwords s = joinWith " " s
unlines :: Array String -> String
unlines s = joinWith "\n" s
untabs :: Array String -> String
untabs s = joinWith "\t" s


showLine :: Maybe Int -> String
showLine (Just x) = show $ x + 1
showLine Nothing = ""
