module Main where

import Prelude

import Data.Array as Array
import Data.Either (either)
import Data.Int as Int
import Data.Maybe (Maybe(..), fromMaybe)
import Data.String as String
import Effect (Effect)
import Effect.Aff (Aff, runAff_, throwError)
import Effect.Class (liftEffect)
import Effect.Exception (throwException, error)
import Node.Encoding (Encoding(..))
import Node.FS.Aff as FS
import Node.Path (FilePath, basenameWithoutExt)
import PureScript.CST (RecoveredParserResult(..), parsePartialModule, parseModule, PartialModule(..))
import PureScript.CST.Print as Print
import PureScript.CST.Range (class TokensOf, tokensOf)
import PureScript.CST.Range.TokenList as TokenList
import PureScript.CST.Traversal (defaultMonoidalVisitor, foldMapModule)
import PureScript.CST.Types (Declaration(..), Module, ModuleHeader(..), SourceToken)
import Effect.Console (log)

import Types (DeclS(..), Result, fromDeclS)
import Utils (showLine, unwords)

foreign import argv :: Array String

takeArgs :: Maybe {loc :: Maybe Int, source:: String, target :: String}
takeArgs = takeArgs1 $ Array.drop 2 argv
  where
    takeArgs1 [a,b,c] = Just {loc: Int.fromString a, source:b, target:c}
    takeArgs1 _ = Nothing

main :: Effect Unit
main = runAff_ (either throwException mempty) do
  {loc,source,target} <- case takeArgs of
                          Just a -> pure a
                          Nothing -> liftEffect $ throwError $ error "needs arguments: loc source target"
  contents <- FS.readTextFile UTF8 source
  res <- runPeel loc contents target
  case res of
       Nothing -> liftEffect $ throwError $ error "no result"
       Just {tokenStart,tokenEnd,resultStr} -> do
          FS.appendTextFile UTF8 target resultStr
          liftEffect $ log $ case tokenStart of
                                  Nothing -> ":" <> show (fromMaybe 1 loc)
                                  Just _ -> showLine tokenStart <> "," <> showLine tokenEnd <> "d"

peelModule :: String -> Maybe FilePath -> Aff String
peelModule _ Nothing = pure ""
peelModule contents (Just target) = do
  case parsePartialModule contents of
    ParseSucceeded (PartialModule m) -> do
      let targetModule = basenameWithoutExt target ".purs"
      let (ModuleHeader t) = m.header
      pure $ append (unwords ["module",targetModule,"where"]) $ String.joinWith "" $ Array.concat $ map (\a -> map Print.printSourceToken $ TokenList.toArray $ tokensOf a) t.imports
    _ -> pure $ "peelModule: error"

runPeel :: Maybe Int -> String -> String -> Aff (Maybe Result)
runPeel loc contents target = do
  targetExist <- FS.exists target
  header <- peelModule contents $ if targetExist then Nothing else Just target
  case parseModule contents of
    ParseSucceeded m -> do
      let (t0 :: (Array (Array SourceToken))) = map (TokenList.toArray <<< tokensOf <<< fromDeclS) <$> fromMaybe [] $ getTarget loc [] $ map (\p -> DeclS p) $ getDecl m
      let tokens = Array.concat t0
          tokenStart = _.range.start.line <$> Array.head tokens
          tokenEnd = _.range.end.line <$> Array.last tokens
      let (t :: (Array String)) = map (String.joinWith "" <<< map Print.printSourceToken) t0
      pure $ Just { tokenStart,tokenEnd, resultStr: String.joinWith "" $ [header] <> t}
    _ -> pure Nothing


getTarget :: Maybe Int -> Array DeclS -> Array DeclS -> (Maybe (Array DeclS))
getTarget Nothing _ _ = Nothing
getTarget (Just iLocation) res x =
  -- iLocation - 1 because index started from 0
  let locFilter f = (map _.range.start.line $ Array.head f) <= Just (iLocation - 1)
      (theToken :: Array (Array SourceToken)) = map (\(DeclS m) -> TokenList.toArray (tokensOf m)) x
      iLoc = Array.findLastIndex locFilter theToken
   in case (join $ Array.index x <$> iLoc) of
        Just a@(DeclS n@(DeclSignature _)) ->
          let allDeclaration = case getTarget (Just (iLocation +1)) [a] x of
                                Just o -> o
                                Nothing -> []
           in Just allDeclaration
           --in Just ([a] <> others)
        Just a@(DeclS n@(DeclValue _)) ->
          let signature = case res of
                [] -> case join $ Array.head <$> getTarget (Just (iLocation - 1)) [] x of
                        Just b@(DeclS (DeclSignature _)) -> [b]
                        _-> []
                r  -> r
              (nameTarget :: Maybe String) = map Print.printSourceToken $ Array.head $ TokenList.toArray $ tokensOf n
              nameOf (DeclS f) = map Print.printSourceToken $ Array.head $ TokenList.toArray $ tokensOf f
              nameFilter f = nameOf a == nameOf f
              others = Array.filter nameFilter x
           in Just (signature <> others)
        Just a -> Just [a]
        Nothing -> Nothing

getDecl :: forall a. TokensOf a => Module a -> (Array (Declaration a))
getDecl = foldMapModule $ defaultMonoidalVisitor
  { onDecl = case _ of
      a -> [a]
  }
