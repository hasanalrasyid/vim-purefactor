PuRefactor
====

puRefactor.vim is a lightweight Vim plugin to refactor purescript statement on to another file.

Demo: <http://www.screenr.com/not_yet>

## Installation

Use [vim-plug](https://github.com/junegunn/vim-plug).

Add puRefactor repository into plugin list. If you use [haskell-vim-now](https://github.com/begriffs/haskell-vim-now.git) as I do,
then it would simply to add a single line in a single file of `~/.config/haskell-vim-now/plugins.vim`

```
Plug 'https://gitlab.com/hasanalrasyid/vim-purefactor.git'
```

Then start vim/nvim, then in command mode, run `:PlugInstall`

If all is well, then on the next occasion when you open a valid purescript source code (using vim/nvim), then you can access `:PuRefactor` command.

## Usage

Just go to any line in the source code, and run `:PuRefactor src/TargetFile.purs`.

The plugins will (hopefully) sends the whole code block to `src/TargetFile.purs` (not only the current line).

If you trust the plugins, and want to automatically remove said code block from the buffer of currently opened file (your current line), you may add this line in `~/.vimrc`.

```
let puRefactor_move = 1
```

## Bugs

If you find a bug, please post it on the issue tracker:
https://gitlab.com/hasanalrasyid/vim-purefactor/issues/

## Contributing

Think you can make this plugin better?  Awesome!

Feel free to send a pull request.

Git repository:    https://gitlab.com/hasanalrasyid/vim-purefactor

## License

MIT/X11.
