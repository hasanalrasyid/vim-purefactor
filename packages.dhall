let upstream =
      https://github.com/purescript/package-sets/releases/download/psc-0.13.8-20210226/packages.dhall sha256:7e973070e323137f27e12af93bc2c2f600d53ce4ae73bb51f34eb7d7ce0a43ea

let additions =
  { language-cst-parser =
      { dependencies =
          [ "arrays"
          , "console"
          , "const"
          , "debug"
          , "effect"
          , "either"
          , "filterable"
          , "foldable-traversable"
          , "free"
          , "functors"
          , "maybe"
          , "numbers"
          , "psci-support"
          , "strings"
          , "transformers"
          , "tuples"
          , "typelevel-prelude"
          ]
      , repo =
          "https://github.com/natefaubion/purescript-language-cst-parser.git"
      , version =
          "c5997dc1bc1c58155fff878ed943022fee974282"
      }

  }
in  upstream // additions
